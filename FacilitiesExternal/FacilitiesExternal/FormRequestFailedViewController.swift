//
//  FormRequestFailedViewController.swift
//  FacilitiesExternal
//
//  Created by Justin Perez on 2/22/20.
//  Copyright © 2020 Andrea Mathew. All rights reserved.
//

import UIKit

class FormRequestFailedViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func closeMessage(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
