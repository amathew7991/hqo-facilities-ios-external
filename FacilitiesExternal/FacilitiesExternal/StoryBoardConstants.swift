//
//  StoryBoardConstants.swift
//  FacilitiesExternal
//
//  Created by Justin Perez on 2/12/20.
//  Copyright © 2020 Andrea Mathew. All rights reserved.
//

public enum STATES {
    static let states = ["",
    "CP - Custon Plate",
    "AK - Alaska",
    "AL - Alabama",
    "AR - Arkansas",
    "AZ - Arizona",
    "CA - California",
    "CO - Colorado",
    "CT - Connecticut",
    "DC - District of Columbia",
    "DE - Delaware",
    "FL - Florida",
    "GA - Georgia",
    "HI - Hawaii",
    "IA - Iowa",
    "ID - Idaho",
    "IL - Illinois",
    "IN - Indiana",
    "KS - Kansas",
    "KY - Kentucky",
    "LA - Louisiana",
    "MA - Massachusetts",
    "MD - Maryland",
    "ME - Maine",
    "MI - Michigan",
    "MN - Minnesota",
    "MO - Missouri",
    "MS - Mississippi",
    "MT - Montana",
    "NC - North Carolina",
    "ND - North Dakota",
    "NE - Nebraska",
    "NH - New Hampshire",
    "NJ - New Jersey",
    "NM - New Mexico",
    "NV - Nevada",
    "NY - New York",
    "OH - Ohio",
    "OK - Oklahoma",
    "OR - Oregon",
    "PA - Pennsylvania",
    "RI - Rhode Island",
    "SC - South Carolina",
    "SD - South Dakota",
    "TN - Tennessee",
    "TX - Texas",
    "UT - Utah",
    "VA - Virginia",
    "VT - Vermont",
    "WA - Washington",
    "WI - Wisconsin",
    "WV - West Virginia",
    "WY - Wyoming"]
};

public enum LICENSE {
    static let oneLetterFiveDigit = ["NV"]
    static let twoLetterFourDigit = ["DC", "ME", "TN"]
    static let threeLetterThreeDigit = ["AK", "AR", "IA", "KS", "KY", "LA", "MN", "ND", "NM", "OK", "OR", "SC", "UT", "VT"]
    static let fourLetterTwoDigit = ["CO", "FL", "NJ"]
    static let sevenDigit = ["NH"]
    static let twoLetterFiveDigit = ["CT", "IL", "MD"]
    static let threeLetterFourDigit = ["AZ", "CA", "GA", "MI", "NC", "NY", "OH", "PA", "TX", "VA", "WA", "WI"]
}
