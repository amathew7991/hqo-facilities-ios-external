//
//  ImageModalController.swift
//  FacilitiesExternal
//
//  Created by ah6z on 3/1/20.
//  Copyright © 2020 Andrea Mathew. All rights reserved.
//

import UIKit

class ImageModalController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    var image: UIImage? = nil;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imageView.image = image;
        // Do any additional setup after loading the view.
    }
    
    @IBAction func close(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
