//
//  ThankYouVC.swift
//  FacilitiesExternal
//
//  Created by Andrea Mathew on 2/1/20.
//  Copyright © 2020 Andrea Mathew. All rights reserved.
//

import UIKit

class ThankYouVC: UIViewController {

 
    @IBOutlet weak var thankYouPopUp: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func closeThankYou(_ sender: Any) {
        thankYouPopUp.isHidden = true
        self.performSegue(withIdentifier: "backToHome", sender: nil)
    }
}
