//
//  ParkingPassTableViewCell.swift
//  FacilitiesExternal
//
//  Created by Andrea Mathew on 2/16/20.
//  Copyright © 2020 Andrea Mathew. All rights reserved.
//

import UIKit

class ParkingPassTableViewCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var creationTime: UILabel!
    @IBOutlet weak var plateNumber: UILabel!
    @IBOutlet weak var yearMakeModel: UILabel!
    @IBOutlet weak var badgeNumber: UILabel!
    @IBOutlet weak var carColor: UIButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func prepareForReuse() {
        super.prepareForReuse()
    }

}
