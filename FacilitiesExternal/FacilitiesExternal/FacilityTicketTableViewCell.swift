//
//  FacilityTicketTableViewCell.swift
//  FacilitiesExternal
//
//  Created by Andrea Mathew on 2/13/20.
//  Copyright © 2020 Andrea Mathew. All rights reserved.
//

import UIKit

class FacilityTicketTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var creationTimeLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var nearMissPlaceholderButton: UIButton!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var imageButton1: UIButton!
    @IBOutlet weak var imageView1: UIImageView!
    @IBOutlet weak var imageButton2: UIButton!
    @IBOutlet weak var imageView2: UIImageView!
    @IBOutlet weak var imageButton3: UIButton!
    @IBOutlet weak var imageView3: UIImageView!
    @IBOutlet weak var noImagesLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        let nearMissImage = UIImage(named: "")
        nearMissPlaceholderButton.setImage(nearMissImage, for: .normal)
        backgroundColor = UIColor.white
        let infoImage = UIImage(named: "outline_info_white_24pt")
        infoButton.setImage(infoImage, for: .normal)
        infoButton.tintColor = UIColor.lightGray
    }
}
