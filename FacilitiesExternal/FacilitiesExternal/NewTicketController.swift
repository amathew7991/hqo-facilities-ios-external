//
//  NewTicketController.swift
//  FacilitiesExternal
//
//  Created by ah6z on 2/3/20.
//  Copyright © 2020 Andrea Mathew. All rights reserved.
//

import UIKit
import AVFoundation
 


class NewTicketController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate
{
    //MARK: Properties
    @IBOutlet weak var imageView1: UIImageView!
    @IBOutlet weak var layerImageView1: UIView!
    @IBOutlet weak var imageView2: UIImageView!
    @IBOutlet weak var layerImageView2: UIView!
    @IBOutlet weak var imageView3: UIImageView!
    @IBOutlet weak var layerImageView3: UIView!
    @IBOutlet weak var qrCodeView: UIView!
    
    @IBOutlet weak var descriptionTextfield: UITextView!
    @IBOutlet weak var descriptionInputContainer: UILabel!
    @IBOutlet weak var nearMissToggle: UISwitch!
    @IBOutlet weak var floorLabel: UILabel!
    @IBOutlet weak var roomLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var addImageButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    
    var descriptionValue = ""
    var isNearMissTicket: Bool = false
    var locationDetails: [String: String] = [:] {
        didSet{
            if(locationDetails != [:]){
                floorLabel.text = "Floor: " + locationDetails["floor"]!
                roomLabel.text = "Room: " + locationDetails["room"]!
                descLabel.text = "Description: " + locationDetails["desc"]!
                qrCodeView.isHidden = false;
            }
        }
    }
    
    //Tutorial
    var inTutorial:Bool = false
    @IBOutlet weak var nearMissHelpMessage: UILabel!
    @IBOutlet weak var nearMissHelpArrow: UIImageView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var closeTutorialButton: UIButton!
    
    // For asynchronous function
    var dispatchGroup = DispatchGroup()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        qrCodeView.isHidden = true;
        layerImageView1.isHidden = true;
        layerImageView2.isHidden = true;
        layerImageView3.isHidden = true;
        //Round corners of of container
        self.descriptionInputContainer.layer.masksToBounds = true
        self.descriptionInputContainer.layer.cornerRadius = 2.5
        //set input text font
        self.descriptionTextfield.font = UIFont(name: "Montserrat-Regular", size: 14)        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")

        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tap)
        
        print("TUTORIAL STATUS: " + String(self.inTutorial)) //remove after tutorial implemented
        self.tutorial()
    }
    
    func showLoadingScreen()
    {
        let alert = UIAlertController(title: "Loading", message: nil, preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 15, y: 15, width: 30, height: 30))
        
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();

        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
    }
    
    //after loading is complete show the Thank You Modal
    func removeLoadingScreen(){
        dismiss(animated: false, completion: showThankYouModal)
    }
    
    func showThankYouModal()
    {
        self.performSegue(withIdentifier: "thankYou", sender: nil)
    }
    
    func tutorial() {
        self.closeTutorialButton.isHidden = true
        self.nearMissHelpMessage.isHidden = true
        self.nearMissHelpArrow.isHidden = true
        self.bottomView.isHidden = true
        if(self.inTutorial) {
            self.closeTutorialButton.isHidden = false
            self.nearMissHelpMessage.isHidden = false
            self.nearMissHelpArrow.isHidden = false
            self.bottomView.isHidden = false
        }
    }
    
    @IBAction func clickCloseTutorial(_ sender: Any) {
        self.inTutorial = false
        //hide tutorial
        self.closeTutorialButton.isHidden = true
        self.nearMissHelpMessage.isHidden = true
        self.nearMissHelpArrow.isHidden = true
        self.bottomView.isHidden = true
    }
    
    
    @IBAction func touchBackButton(_ sender: Any) {
        self.performSegue(withIdentifier: "backToHome", sender: nil)
    }
    
    
    @IBAction func clearLocationDetails(_ sender: Any) {
        qrCodeView.isHidden = true;
        locationDetails = [:]
    }
    
    @IBAction func clearImage1(_ sender: Any) {
        layerImageView1.isHidden = true;
        imageView1.image = nil
        addImageButton.isEnabled = true;
    }
    
    @IBAction func clearImage2(_ sender: Any) {
        layerImageView2.isHidden = true;
        imageView2.image = nil
        addImageButton.isEnabled = true;
    }
    
    @IBAction func clearImage3(_ sender: Any) {
        layerImageView3.isHidden = true;
        imageView3.image = nil
        addImageButton.isEnabled = true;
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

    
    @IBAction func takePhoto(_ sender: Any) {
        let imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a source", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                imagePicker.sourceType = .camera;
                self.present(imagePicker, animated: true, completion: nil)
            } else {
                print("Camera not available")
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in imagePicker.sourceType = .photoLibrary; self.present(imagePicker, animated: true, completion: nil)}))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil ))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        let image = info[.originalImage] as! UIImage
        if(imageView1.image == nil){
            imageView1.image = info[.originalImage] as? UIImage
            layerImageView1.isHidden = false;
        } else if(imageView2.image == nil){
            imageView2.image = info[.originalImage] as? UIImage
            layerImageView2.isHidden = false;
        } else if(imageView3.image == nil){
            imageView3.image = info[.originalImage] as? UIImage
            layerImageView3.isHidden = false;
        } else {
            print("Cannot add more photos")
        }
        if(imageView1.image != nil && imageView2.image != nil && imageView3.image != nil){
            addImageButton.isEnabled = false;
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    //First compress image, then conver to base64 string
    func toBase64( image: UIImage ) -> String?{
        let imageCompress = image.jpegData(compressionQuality: 0.50)
        return (imageCompress?.base64EncodedString())!
    }
    
    @IBAction func submitTicket(_ sender: Any) {
        
        // save data in the textfield
        descriptionValue = descriptionTextfield.text!
        
        print("DESCRIPTION: " + descriptionValue)
        print(locationDetails)
        
        var images = [String]()
        if(imageView1.image != nil){
            let imageString1: String? = self.toBase64(image: imageView1.image!)
            images.append(imageString1!)
        }
        if(imageView2.image != nil){
            let imageString2: String? = self.toBase64(image: imageView2.image!)
            images.append(imageString2!)
        }
        if(imageView3.image != nil){
            let imageString3: String? = self.toBase64(image: imageView3.image!)
            images.append(imageString3!)
        }
        
        //get state of near miss toggle
        self.isNearMissTicket =  self.nearMissToggle.isOn
        
        if(descriptionValue != "") {
            //Setting up connection
            let createTicketEndpoint: String = "https://modular-ground-255216.appspot.com/create_ticket"
            //        For Jose's Local Changes
            //let createTicketEndpoint: String = "http://127.0.0.1:8080/create_ticket"
            guard let url = URL(string: createTicketEndpoint) else {
            print("Error: cannot create URL")
                return
            }

            var urlRequest = URLRequest(url: url)
            urlRequest.httpMethod = "POST"

            let json = [
              "user_id": "asm375",
              "description":descriptionValue,
              "location_details": locationDetails,
              "images": images,
              "is_near_miss_opportunity":self.isNearMissTicket,
            ] as [String : Any]
            
            let jsonData = try? JSONSerialization.data(withJSONObject: json)
            
            urlRequest.httpBody = jsonData

            urlRequest.setValue("\(jsonData?.count)", forHTTPHeaderField: "Content-Length")
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            urlRequest.setValue("max=5", forHTTPHeaderField: "Keep-Alive")

            let session = URLSession.shared
            
            self.dispatchGroup.enter()
            self.showLoadingScreen()
            self.submitButton.isEnabled = false;
            
            let task = session.dataTask(with: urlRequest) {
                (data, response, error) in
                // check for any errors
                guard error == nil else {
                  print("error calling POST")
                  print(error!)
                  return
                }
                // make sure we got data
                guard let responseData = data else {
                    print("Error: did not receive data")
                    return
                }
                print(responseData)
                // parse the result as JSON, since that's what the API provides
                do {
                    let response = try JSONSerialization.jsonObject(with: responseData, options: [])
                    print(response)
                    self.dispatchGroup.leave()
                } catch  {
                    self.submitButton.isEnabled = true;
                    print("error trying to convert data to JSON")
                    return
                }
            }
            task.resume()
                        
            self.dispatchGroup.notify(queue: .main) {
                self.removeLoadingScreen()
            }
            
        } else {
            print("NULL DESCRIPTION")
            self.performSegue(withIdentifier: "enterDescription", sender: nil)
        }
    }
}
    

