//
//  EmptyDescriptionViewController.swift
//  FacilitiesExternal
//
//  Created by Andrea Mathew on 2/16/20.
//  Copyright © 2020 Andrea Mathew. All rights reserved.
//

import UIKit

class EmptyDescriptionViewController: UIViewController {

    @IBOutlet weak var modalContainter: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func closeDescWarning(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
