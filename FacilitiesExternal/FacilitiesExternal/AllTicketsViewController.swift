//
//  AllTicketsViewController.swift
//  FacilitiesExternal
//
//  Created by Andrea Mathew on 2/12/20.
//  Copyright © 2020 Andrea Mathew. All rights reserved.
//

import UIKit

struct FacilityTicket {
    let description: String
    let creation_time: String
    let location_details: [String: Any]
    let is_near_miss_opportunity: Bool
    let is_completed:Bool
    let images: [String]
    
    init(json: [String: Any]) {
        description = json["description"] as? String ?? ""
        creation_time = json["creation_time"] as? String ?? ""
        location_details = json["location_details"] as? [String: Any] ?? [:]
        is_near_miss_opportunity = json["is_near_miss_opportunity"] as? Bool ?? false
        is_completed = json["is_completed"] as? Bool ?? false
        images = json["images"] as? [String] ?? []
    }
}

struct Location {
    let building: String
    let desc: String
    let floor: String
    let room: String
    
    init(json: [String: Any]) {
        building = json["building"] as? String ?? ""
        desc = json["desc"] as? String ?? ""
        floor = json["floor"] as? String ?? ""
        room = json["room"] as? String ?? ""
    }
}

struct ParkingPass {
    let first_name:String
    let last_name:String
    let creation_date:String
    let license_plate:String
    let year:Int
    let make:String
    let model:String
    let badge_no:String
    let color:String
    
    init(json: [String: Any]) {
        first_name = json["first_name"] as? String ?? ""
        last_name = json["last_name"] as? String ?? ""
        creation_date = json["creation_date"] as? String ?? ""
        license_plate = json["license_plate"] as? String ?? ""
        year = json["year"] as? Int ?? 0
        make = json["make"] as? String ?? ""
        model = json["model"] as? String ?? ""
        badge_no = json["badge_no"] as? String ?? ""
        color = json["color"] as? String ?? ""
    }
}

struct Vehicle {
    let year: String
    let make: String
    let model: String
}

class AllTicketsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NSURLConnectionDelegate, XMLParserDelegate {
    
    //Tutorial
    var inTutorial:Bool = false
    @IBOutlet weak var tutorialBackground: UIView!
    
    
    @IBOutlet weak var tableView: UITableView!
    
    var tableDataTypes:[String] = []
    
    //Facility Ticket
    var allTicketsInfo: NSArray! = []
    var numTickets: Int = 0
    var ticketDescriptions: [String] = []
    var ticketCreationTimes: [String] = []
    var ticketLocations: [String] = []
    var nearMissTickets: [Bool] = []
    var completedTickets: [Bool] = []
    var images: [[UIImage]] = []
    
    //Parking Pass
    var allParkingPassInfo: NSArray! = []
    var numParkingPasses: Int = 0
    var parkingPassNames: [String] = []
    var parkingPassCreationTimes: [String] = []
    var licensePlates: [String] = []
    var yearMakeModels: [String] = []
    var badgeNumbers: [String] = []
    var carColors: [String] = []
    var carYears: [String] = []
    var carDescription: [String] = []
    
    //expand tickets
    var selectedIndex = -1
    var isCollapsed = false
    

    //vehicle registration
    var mutableData:NSMutableData  = NSMutableData()
    var vehicles: [Vehicle] = []
    var currentParsingElement: String = String()
    var year = String()
    var make = String()
    var model = String()

    // For asynchronous function
    var dispatchGroup = DispatchGroup()
    // ensure to only show when the page is loading
    var isLoading = true

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.layer.cornerRadius = 5.0
        self.tableView.tableFooterView = UIView()
        
        //Set tutorial
        self.tutorialBackground.isHidden = true
        
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.estimatedRowHeight = 200
        self.tableView.rowHeight = UITableView.automaticDimension
        
        self.parseOutAllTickets()
        
        self.dispatchGroup.notify(queue: .main) {
            self.tableView.reloadData()
            self.removeLoadingScreen()
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if self.isLoading {
            showLoadingScreen()
        }
    }
    
    // Loading screen
    func showLoadingScreen()
    {
        let alert = UIAlertController(title: "Loading", message: nil, preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 15, y: 15, width: 30, height: 30))
        
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();

        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
    }
    
    func removeLoadingScreen(){
        self.isLoading = false
        dismiss(animated: false)
    }
    
    func parseOutAllTickets(){
        self.dispatchGroup.enter()
        let getTicketsEndpoint:String = "https://modular-ground-255216.appspot.com/get_tickets?user_id=asm375"
        guard let url = URL(string: getTicketsEndpoint) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            guard let data = data else { return }
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary
                if let parseJSON = json {
                    //only get the facility tickets array
                    let ticketData: NSDictionary = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! NSDictionary
                    self.allTicketsInfo = ticketData.value(forKey: "tickets") as! NSArray
                    //only get parking passes array
                    let parkingPassData: NSDictionary = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! NSDictionary
                    self.allParkingPassInfo = parkingPassData.value(forKey: "parking_passes") as! NSArray
                    
                    self.buildTableData()
                    self.getTicketInfo()
                    self.getParkingPassInfo()
                    self.dispatchGroup.leave()
                }
            } catch let jsonErr {
                print("Error serializing json: ", jsonErr)
            }
        }.resume()
    }
    
    func buildTableData() {
        // helper function to determine types of cells needed at instantiation
        let facilityTicketCount:Int = self.allTicketsInfo.count
        //TODO check for loaded tickets
        if(facilityTicketCount > 0){
            for _ in 1...facilityTicketCount {
                self.tableDataTypes.append("Facility")
            }
        }
        let parkingPassTicketCount:Int = self.allParkingPassInfo.count
        if(parkingPassTicketCount > 0) {
            for _ in 1...parkingPassTicketCount {
                self.tableDataTypes.append("Parking Pass")
            }
        }
    }
    
    func getTicketInfo() {
        // get description, creation time, location, near miss, completed
        var collectDescriptions: [String] = []
        var collectCreationTimes: [String] = []
        var collectLocations: [String] = []
        var collectNearMiss: [Bool] = []
        var collectCompleted: [Bool] = []
        var collectImages: [[UIImage]] = []
        for ticket in self.allTicketsInfo {
            //ticket represents JSON for one single ticket with its info
            guard let json = ticket as? [String: Any] else {return}
            let facilityTicket = FacilityTicket(json: json)
            let description = facilityTicket.description
            let creation_time = self.formatTime(unformattedTime: facilityTicket.creation_time)
            //format creation time
            guard let locationJSON = facilityTicket.location_details as? [String: Any] else {return}
            let locationStruct = Location(json: locationJSON)
            var location = ""
            if locationStruct.floor != "" && locationStruct.room != ""{
                location = "Floor " + locationStruct.floor + " / " + locationStruct.room
            }
            let nearMissValue = facilityTicket.is_near_miss_opportunity
            let isCompleted = facilityTicket.is_completed
            
            // Decode all images
            var ticketImages: [UIImage] = []
            for image in facilityTicket.images {
                let dataDecoded : Data = Data(base64Encoded: image, options: .ignoreUnknownCharacters)!
                let decodedImage = UIImage(data: dataDecoded)
                ticketImages.append(decodedImage!)
            }
            
            collectDescriptions.insert(description, at: 0)
            collectCreationTimes.insert(creation_time, at: 0)
            collectLocations.insert(location, at: 0)
            collectNearMiss.insert(nearMissValue, at: 0)
            collectCompleted.insert(isCompleted, at: 0)
            collectImages.insert(ticketImages, at: 0)
        }
        self.ticketDescriptions = collectDescriptions
        self.ticketCreationTimes = collectCreationTimes
        self.ticketLocations = collectLocations
        self.nearMissTickets = collectNearMiss
        self.completedTickets = collectCompleted
        self.images = collectImages
    }
    
    //Returns correctly formatted time stamp
    func formatTime(unformattedTime:String) -> String {
        var formattedTime:String = ""
        print("FORMATTED TIME")
        if let range = unformattedTime.range(of: ", "){
            formattedTime = String(unformattedTime[range.upperBound...])
        }
        // day, month, year @ time
        return formattedTime
    }
    
    func getParkingPassInfo() {
        //get first name, last name, creation date,
        var collectNames: [String] = []
        var collectCreationDates: [String] = []
        var collectLicensePlateNumber: [String] = []
        var collectYearMakeModel: [String] = []
        var collectBadgeNumber: [String] = []
        var collectCarColor: [String] = []
        for pass in self.allParkingPassInfo {
            guard let json = pass as? [String: Any] else { return }
            let parkingPass = ParkingPass(json: json)
            let name = parkingPass.first_name + " " + parkingPass.last_name
            let creation_date = self.formatTime(unformattedTime: parkingPass.creation_date)
            let license_plate = parkingPass.license_plate
            let yearMakeModel = String(parkingPass.year) + " " + parkingPass.make
            let badgeNumber = parkingPass.badge_no
            let carColor = parkingPass.color
            collectNames.insert(name, at:0)
            collectCreationDates.insert(creation_date, at: 0)
            collectLicensePlateNumber.insert(license_plate, at: 0)
            collectYearMakeModel.insert(yearMakeModel, at: 0)
            collectBadgeNumber.insert(badgeNumber, at: 0)
            collectCarColor.insert(carColor, at: 0)
        }
        self.parkingPassNames = collectNames
        self.parkingPassCreationTimes = collectCreationDates
        self.licensePlates = collectLicensePlateNumber
        self.yearMakeModels = collectYearMakeModel
        self.badgeNumbers = collectBadgeNumber
        self.carColors = collectCarColor
        
    }
    
    @IBAction func clickCreateFacilityTicket(_ sender: Any) {
        self.performSegue(withIdentifier: "createFacilityTicket", sender: nil)
        
    }
    
    @IBAction func clickCreateParkingPass(_ sender: Any) {
        self.performSegue(withIdentifier: "createParkingPass", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Destination is new ticket controller
        if segue.identifier == "createFacilityTicket" {
            let vc = segue.destination as! NewTicketController
            vc.inTutorial = self.inTutorial
        } else if segue.identifier == "createParkingPass" {
            let vc = segue.destination as! RequestParkingPassViewController
            vc.inTutorial = self.inTutorial
        } else if segue.identifier == "imageView1" {
            let vc = segue.destination as! ImageModalController
            let imageIndex = self.selectedIndex
            vc.image = images[imageIndex][0]
        } else if segue.identifier == "imageView2" {
            let vc = segue.destination as! ImageModalController
            let imageIndex = self.selectedIndex
            vc.image = self.images[imageIndex][1]
        } else if segue.identifier == "imageView3" {
            let vc = segue.destination as! ImageModalController
            let imageIndex = self.selectedIndex
            vc.image = self.images[imageIndex][2]
        }
        
    }
    
    @IBAction func clickHelp(_ sender: Any) {
        if(self.inTutorial){
            self.inTutorial = false
            //hide icons
            self.tutorialBackground.isHidden = true
            
        } else {
            self.inTutorial = true
            //show icons
            self.tutorialBackground.isHidden = false
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.selectedIndex == indexPath.row && isCollapsed == true {
            return 200
        } else {
            return 90
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //update this to return all tickets
        self.numTickets = self.allTicketsInfo.count + self.allParkingPassInfo.count
        return self.numTickets
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cellType:String = self.tableDataTypes[indexPath.row]
        
        if cellType == "Parking Pass" {
            let diff:Int = indexPath.row - self.allTicketsInfo.count
            let cell: ParkingPassTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "ParkingPass") as! ParkingPassTableViewCell
            cell.separatorInset = UIEdgeInsets.zero
            cell.layoutMargins = UIEdgeInsets.zero
            cell.name.text = self.parkingPassNames[diff]
            cell.creationTime.text = self.parkingPassCreationTimes[diff]
            cell.plateNumber.text = self.licensePlates[diff]
            cell.yearMakeModel.text = self.yearMakeModels[diff]
            cell.badgeNumber.text = self.badgeNumbers[diff]
            let color = self.carColors[diff]
            switch color {
            case "black":
                cell.carColor.backgroundColor = UIColor.black
            case "white":
                cell.carColor.backgroundColor = UIColor.white
                cell.carColor.layer.borderWidth = 1.0
                cell.carColor.layer.borderColor = UIColor.black.cgColor
            case "gray":
                cell.carColor.backgroundColor = UIColor.gray
            case "brown":
                cell.carColor.backgroundColor = UIColor(red:0.37, green:0.20, blue:0.05, alpha:1.0)
            case "red":
                cell.carColor.backgroundColor = UIColor(red:0.78, green:0.18, blue:0.18, alpha:1.0)
            case "orange":
                cell.carColor.backgroundColor = UIColor(red:1.00, green:0.51, blue:0.16, alpha:1.0)
            case "yellow":
                cell.carColor.backgroundColor = UIColor(red:0.97, green:0.88, blue:0.10, alpha:1.0)
            case "green":
                cell.carColor.backgroundColor = UIColor(red:0.33, green:0.73, blue:0.29, alpha:1.0)
            case "blue":
                cell.carColor.backgroundColor = UIColor(red:0.19, green:0.21, blue:0.74, alpha:1.0)
            case "purple":
                cell.carColor.backgroundColor = UIColor(red:0.49, green:0.20, blue:0.71, alpha:1.0)
            default:
                cell.carColor.backgroundColor = UIColor.white
            }
            
            return cell
            
        } else {
            // facility ticket type
            let ticketDescription = self.ticketDescriptions[indexPath.row]
            let ticketCreationTimeAndDate = self.ticketCreationTimes[indexPath.row]
            let ticketLocation = self.ticketLocations[indexPath.row]
            let isNearMissTicket = self.nearMissTickets[indexPath.row]
            let isCompletedTicket = self.completedTickets[indexPath.row]
            let images = self.images[indexPath.row]
            
            let cell:FacilityTicketTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FacilityTicket") as! FacilityTicketTableViewCell
            cell.separatorInset = UIEdgeInsets.zero
            cell.layoutMargins = UIEdgeInsets.zero
                 //Configure the cell's contents with the row and section number
            cell.descriptionLabel.text = ticketDescription
            cell.creationTimeLabel.text = ticketCreationTimeAndDate
            cell.locationLabel.text = ticketLocation
            
            cell.noImagesLabel.isHidden = false;
            cell.imageView1.image = nil;
            cell.imageButton1.isHidden = true;
            cell.imageView2.image = nil;
            cell.imageButton2.isHidden = true;
            cell.imageView3.image = nil;
            cell.imageButton3.isHidden = true;
            
            if(images.count > 0){
                cell.noImagesLabel.isHidden = true;
                for (index, element) in images.enumerated() {
                    if(index == 0){
                        cell.imageView1.image = element;
                        cell.imageButton1.isHidden = false;
                    }else if(index == 1){
                        cell.imageView2.image = element;
                        cell.imageButton2.isHidden = false;
                    }else if(index == 2){
                        cell.imageView3.image = element;
                        cell.imageButton3.isHidden = false;
                    }
                }
            }
            
            if(isNearMissTicket){
                //set the image
                let image = UIImage(named: "outline_warning_white_24pt.imageset")
                cell.nearMissPlaceholderButton.setImage(image, for: .normal)
            }
            
            if(isCompletedTicket){
                //set background and green checkmark
                cell.backgroundColor = UIColor(red: 9/255, green: 186/255, blue: 166/255, alpha: 0.25)
                //cell.backgroundColor = UIColor.green
                let completeImage = UIImage(named: "baseline_check_circle_white_24pt.imageset")
                cell.infoButton.setImage(completeImage, for: .normal)
                cell.infoButton.tintColor = UIColor(red: 9/255, green: 186/255, blue: 166/255, alpha: 1)
            }
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if self.selectedIndex == indexPath.row {
            if self.isCollapsed == false {
                self.isCollapsed = true
            } else {
                self.isCollapsed = false
            }
        } else {
            self.isCollapsed = true
        }
        self.selectedIndex = indexPath.row
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
}

