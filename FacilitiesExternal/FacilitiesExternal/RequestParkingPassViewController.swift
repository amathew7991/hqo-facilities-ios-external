//
//  RequestParkingPassViewController.swift
//  FacilitiesExternal
//
//  Created by Justin Perez on 2/12/20.
//  Copyright © 2020 Andrea Mathew. All rights reserved.
//

import UIKit

class RequestParkingPassViewController:
    UIViewController,
    UIImagePickerControllerDelegate,
    UINavigationControllerDelegate,
    UITextFieldDelegate,
    UIPickerViewDataSource,
    UIPickerViewDelegate,
    UIScrollViewDelegate,
    NSURLConnectionDelegate,
    XMLParserDelegate
    {
    
    
    @IBOutlet weak var addOrRemoveHandicapImageButton: UIButton!
    @IBOutlet weak var blackButton: UIButton!
    @IBOutlet weak var blueButton: UIButton!
    @IBOutlet weak var brownButton: UIButton!
    @IBOutlet weak var grayButton: UIButton!
    @IBOutlet weak var greenButton: UIButton!
    @IBOutlet weak var orangeButton: UIButton!
    @IBOutlet weak var redButton: UIButton!
    @IBOutlet weak var purpleButton: UIButton!
    @IBOutlet weak var whiteButton: UIButton!
    @IBOutlet weak var yellowButton: UIButton!
    
    @IBOutlet weak var addHandicapImageLabel: UILabel!
    @IBOutlet weak var contactInfoTextFieldContainer: UILabel!
    @IBOutlet weak var idTextFieldContainer: UILabel!
    @IBOutlet weak var licenseTextFieldContainer: UILabel!
    @IBOutlet weak var preferredContactLabel: UILabel!
    @IBOutlet weak var statePickerTextFieldContainer: UILabel!
    
    
    @IBOutlet weak var licensePlateScannerButton: UIButton!
    
    
    @IBOutlet weak var alternateContactInfoIcon: UIImageView!
    @IBOutlet weak var handicapImage: UIImageView!
    
    
    @IBOutlet weak var scrollContentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var preferredContactMethodSegmentControl: UISegmentedControl!
    
    @IBOutlet weak var electricVehicleSwitch: UISwitch!
    @IBOutlet weak var handicapSwitch: UISwitch!
    @IBOutlet weak var multipleVehicleSwitch: UISwitch!
    
    @IBOutlet weak var contactInfoTextField: UITextField!
    @IBOutlet weak var idTextField: UITextField!
    @IBOutlet weak var licenseTextField: UITextField!
    @IBOutlet weak var statePickerTextField: UITextField!
    
    @IBOutlet weak var submitButton: UIButton!
    
    let BLACK = "black"
    let BLUE = "blue"
    let BROWN = "brown"
    let GRAY = "gray"
    let GREEN = "green"
    let ORANGE = "orange"
    let RED = "red"
    let PURPLE = "purple"
    let WHITE = "white"
    let YELLOW = "yellow"
    
    let EMAIL = "email"
    let PHONE = "phone"
    let TEXT = "text"
    
    let HANDICAP_IMAGE_HEIGHT = 110
    let SCROLL_VIEW_HEIGHT = 675
    let SCROLL_VIEW_WIDTH = 375
    
    var imagePicker: UIImagePickerController!
    
    var selectedColor : String = ""
    var selectedContactMethod : String = ""
    var selectedState : String = ""
    
    var isDeletingHandicapImage = false
    var validBadge = false
    var validLicense = false
    var validState = false
    var validColor = false
    var validContactMethod = false
    var validContactInfo = false
    var year:String = String()
    var make:String = String()
    
    //Tutorial
    var inTutorial:Bool = false
    @IBOutlet weak var tutorialTopView: UIView!
    @IBOutlet weak var closeTutorialButton: UIButton!
    
    //vehicle registration
    var currentParsingElement: String = String()
    // For asynchronous function
    var dispatchGroup = DispatchGroup()

    
    override func viewDidLoad() {
        super.viewDidLoad();
        
        scrollContentView.translatesAutoresizingMaskIntoConstraints = true
        
        self.idTextField.delegate = self
        self.licenseTextField.delegate = self
        self.statePickerTextField.delegate = self
        self.contactInfoTextField.delegate = self
        self.scrollView.delegate = self

        scrollView.keyboardDismissMode = .onDrag
        addHandicapImageLabel.layer.borderWidth = 2.5
        addHandicapImageLabel.layer.borderColor = CGColorFromHex(rgbValue: 0xFAFAFA, alpha: 1.0)
        
        //Round corners
        self.idTextFieldContainer.layer.masksToBounds = true
        self.licenseTextFieldContainer.layer.masksToBounds = true
        self.statePickerTextFieldContainer.layer.masksToBounds = true
        self.contactInfoTextFieldContainer.layer.masksToBounds = true
        self.licensePlateScannerButton.layer.masksToBounds = true
        self.idTextFieldContainer.layer.cornerRadius = 2.5
        self.licenseTextFieldContainer.layer.cornerRadius = 2.5
        self.contactInfoTextFieldContainer.layer.cornerRadius = 2.5
        self.statePickerTextFieldContainer.layer.cornerRadius = 2.5
        self.licensePlateScannerButton.layer.cornerRadius = 2.5
        
        
        //Set text input font
        self.idTextField.font = UIFont(name: "Montserrat-Regular", size: 16)
        self.licenseTextField.font = UIFont(name: "Montserrat-Regular", size: 14)
        self.statePickerTextField.font = UIFont(name: "Montserrat-Regular", size: 16)
        self.contactInfoTextField.font = UIFont(name: "Montserrat-Regular", size: 14)
        
        // makes handicap image cell clickable (only when adding)
        let handicapTap = UITapGestureRecognizer(target: self, action: #selector(RequestParkingPassViewController.addHandicapImageLabelClickable))
        addHandicapImageLabel.isUserInteractionEnabled = true
        addHandicapImageLabel.addGestureRecognizer(handicapTap)
        
        // Oberservers for keyboard
        // Will push up text fields to prevent keyboard from covering them
        NotificationCenter.default.addObserver(
          self,
          selector: #selector(keyboardWillShow(_:)),
          name: UIResponder.keyboardWillShowNotification,
          object: nil)

        NotificationCenter.default.addObserver(
          self,
          selector: #selector(keyboardWillHide(_:)),
          name: UIResponder.keyboardWillHideNotification,
          object: nil)
        
        addToolBarToKeyboard()
        createPickerView()
        hideAddHandicapImage()
        setPlaceholderText()
        
        //Tutorial
        self.tutorial()
    }

    override func viewDidLayoutSubviews() {
        scrollView.contentSize = CGSize(width: SCROLL_VIEW_WIDTH, height: SCROLL_VIEW_HEIGHT)
    }
    
    func tutorial(){
        self.closeTutorialButton.isHidden = true
        self.tutorialTopView.isHidden = true
        if(self.inTutorial) {
            self.closeTutorialButton.isHidden = false
            self.tutorialTopView.isHidden = false
        }
    }
    
    @IBAction func clickCloseTutorial(_ sender: Any) {
        self.inTutorial = false
        self.closeTutorialButton.isHidden = true
        self.tutorialTopView.isHidden = true
    }
    
    // Take picture then detect license plate
    @IBAction func detectLicensePlate(_ sender: Any) {
    }
    
    // FORM VALIDATIONS
    
    // number of characters allowed in each text field
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField.tag == 1) // Badge Number Text Field
        {
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)

            return updatedText.count <= 9
        }
        else if(textField.tag == 2) // License Plate Field
        {
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)

            return updatedText.count <= 7
        }
        else{ // default
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)

            return updatedText.count <= 64
        }
    }
    
    // Validates various text fields
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool
    {
        switch textField.tag {
        case 1: // Badge Number Text Field
            let currentText = textField.text ?? ""
            isBadgeNumberValid(currentText) ? inputIsValid(idTextFieldContainer) :              inputIsInvalid(idTextFieldContainer)
            break
        case 2: // License Plate Text Field
            isLicenseValidByState(selectedState) ? inputIsValid(licenseTextFieldContainer) : inputIsInvalid(licenseTextFieldContainer)
            break
        case 3: // State Text Field
            let currentText = textField.text ?? ""
            isStateValid(currentText) ? inputIsValid(statePickerTextFieldContainer) : inputIsInvalid(statePickerTextFieldContainer)
            
            // Re-validate License if State is changed
            if(licenseTextField.text != "")
            {
                isLicenseValidByState(selectedState) ? inputIsValid(licenseTextFieldContainer) : inputIsInvalid(licenseTextFieldContainer)
            }
            break
        case 4: // Contact Info Text Field
            let currentText = textField.text ?? ""
            isContactInfoValid(currentText) ? inputIsValid(contactInfoTextFieldContainer) : inputIsInvalid(contactInfoTextFieldContainer)
            break
        default:
            return true
        }
        
        return true
    }
    
    func isBadgeNumberValid(_ badge: String) -> Bool
    {
        if(badge.isInt && badge.count == 9)
        {
            validBadge = true
            return true
        }
        else{
            validBadge = false
            return false
        }
    }
    
    // See StoryBoardConstants.swift for different license plate validations
    func isLicenseValidByState(_ state: String) -> Bool
    {
        let licenseText = licenseTextField.text!
        let stateText = statePickerTextField.text!
        
        if(licenseText == "")
        {
            validLicense = false
            return false
        }
        else if(stateText == "" || stateText == "CP" ) //if the user puts in the license plate before the state, it may be valid
        {
            if(licenseText.count == 6 || licenseText.count == 7)
            {
                validLicense = true
                return true
            }
            else{
                validLicense = false
                return false
            }
        }
        else if(licenseText.count == 6)
        {
            if(LICENSE.oneLetterFiveDigit.contains(state))
            {
                if(licenseText.matches("^[A-Za-z]{1}[0-9]{5}$"))
                {
                    validLicense = true
                    return true
                }
                else{
                    validLicense = false
                    return false
                }
            }
            else if(LICENSE.twoLetterFourDigit.contains(state))
            {
                if(licenseText.matches("^[A-Za-z]{2}[0-9]{4}$"))
                {
                    validLicense = true
                    return true
                }
                else{
                    validLicense = false
                    return false
                }
            }
            else if(LICENSE.threeLetterThreeDigit.contains(state))
            {
                if(licenseText.matches("^[A-Za-z]{3}[0-9]{3}$"))
                {
                    validLicense = true
                    return true
                }
                else{
                    validLicense = false
                    return false
                }
            }
            else if(LICENSE.fourLetterTwoDigit.contains(state))
            {
                if(licenseText.matches("^[A-Za-z]{4}[0-9]{2}$"))
                {
                    validLicense = true
                    return true
                }
                else
                {
                    validLicense = false
                    return false
                }
            }
            else{
                validLicense = false
                return false
            }
        }
        else if(licenseText.count == 7)
        {
            if(LICENSE.sevenDigit.contains(state))
            {
                if(licenseText.matches("^[0-9]{7}$"))
                {
                    validLicense = true
                    return true
                }
                else{
                    validLicense = false
                    return false
                }
            }
            else if(LICENSE.twoLetterFiveDigit.contains(state))
            {
                if(licenseText.matches("^[A-Za-z]{2}[0-9]{5}$"))
                {
                    validLicense = true
                    return true
                }
                else{
                    validLicense = false
                    return false
                }
            }
            else if(LICENSE.threeLetterFourDigit.contains(state))
            {
                if(licenseText.matches("^[A-Za-z]{3}[0-9]{4}$"))
                {
                    validLicense = true
                    return true
                }
                else{
                    validLicense = false
                    return false
                }
            }
            else{
                validLicense = false
                return false
            }
        }
        else{
            validLicense = false
            return false
        }
    }

    func isStateValid(_ state: String) -> Bool
    {
        if(state != "")
        {
            validState = true
            return true
        }
        else{
            return false
        }
    }
    
    func isContactInfoValid(_ contact: String) -> Bool
    {
        if(selectedContactMethod == EMAIL)
        {
            if(contact.matches("[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"))
            {
                validContactInfo = true
                return true
            }
            else{
                validContactInfo = false
                return false
            }
        }
        else if(selectedContactMethod == PHONE || selectedContactMethod == TEXT)
        {
            if(contact.matches("[([+]?1+[-]?)?+([(]?+([0-9]{3})?+[)]?)?+[-]?+[0-9]{3}+[-]?+[0-9]{4}]"))
            {
                validContactInfo = true
                return true
            }
            else{
                validContactInfo = false
                return false
            }
        }
        else{
            validContactInfo = false
            return false
        }
    }
    
    // UI HELPER FUNCTIONS
    
    func CGColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->CGColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
     
        return CGColor(srgbRed:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    
    func inputIsValid(_ inputContainer: UILabel)
    {
        inputContainer.layer.borderWidth = 2.5
        inputContainer.layer.borderColor = UIColor.green.cgColor
    }
    
    func inputIsInvalid(_ inputContainer: UILabel)
    {
        inputContainer.layer.borderWidth = 2.5
        inputContainer.layer.borderColor = UIColor.red.cgColor
    }
    
    func addToolBarToKeyboard() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Done", style: .plain, target: self,
                                     action: #selector(self.endEditing))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        
        idTextField.inputAccessoryView = toolBar
        licenseTextField.inputAccessoryView = toolBar
        statePickerTextField.inputAccessoryView = toolBar
        contactInfoTextField.inputAccessoryView = toolBar
    }
    
    @objc func endEditing() {
        view.endEditing(true)
    }
    
    func clearAndSetColorButtonHighlight(_ colorButton: UIButton)
    {
        blackButton.layer.borderWidth = 0
        whiteButton.layer.borderWidth = 0
        grayButton.layer.borderWidth = 0
        brownButton.layer.borderWidth = 0
        redButton.layer.borderWidth = 0
        orangeButton.layer.borderWidth = 0
        yellowButton.layer.borderWidth = 0
        greenButton.layer.borderWidth = 0
        blueButton.layer.borderWidth = 0
        purpleButton.layer.borderWidth = 0
        colorButton.layer.borderWidth = 2.5
        colorButton.layer.borderColor = UIColor.green.cgColor
    }
    
    // Lock horizontal scrolling
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollView.contentOffset.x = 0
    }
    
    // HANDICAP IMAGE
    
    func showAddHandicapImage()
    {
        preferredContactLabel.frame = CGRect(x: 0.0, y: 531.0, width: preferredContactLabel.frame.width, height: preferredContactLabel.frame.height)
        preferredContactMethodSegmentControl.frame = CGRect(x: 190.0, y: 527, width: preferredContactMethodSegmentControl.frame.width, height: preferredContactMethodSegmentControl.frame.height)
        contactInfoTextFieldContainer.frame = CGRect(x: 0.0, y: 573.0, width: contactInfoTextFieldContainer.frame.width, height: contactInfoTextFieldContainer.frame.height)
        alternateContactInfoIcon.frame = CGRect(x: 10.0, y: 588.0, width: alternateContactInfoIcon.frame.width, height: alternateContactInfoIcon.frame.height)
        contactInfoTextField.frame = CGRect(x: 42.0, y: 584.0, width: contactInfoTextField.frame.width, height: contactInfoTextField.frame.height)
        
        addOrRemoveHandicapImageButton.isHidden = false
        handicapImage.isHidden = false
        
        // Only show label when there is no image
        if(handicapImage.image == nil)
        {
            addHandicapImageLabel.isHidden = false
        }
    }
    
    func hideAddHandicapImage()
    {
        // Only show label when there is no image
        if(handicapImage.image == nil)
        {
            addHandicapImageLabel.isHidden = true
        }
        addOrRemoveHandicapImageButton.isHidden = true
        handicapImage.isHidden = true
        
        preferredContactLabel.frame = CGRect(x: 0.0, y: 431.0, width: preferredContactLabel.frame.width, height: preferredContactLabel.frame.height)
        preferredContactMethodSegmentControl.frame = CGRect(x: 190.0, y: 427, width: preferredContactMethodSegmentControl.frame.width, height: preferredContactMethodSegmentControl.frame.height)
        contactInfoTextFieldContainer.frame = CGRect(x: 0.0, y: 473.0, width: contactInfoTextFieldContainer.frame.width, height: contactInfoTextFieldContainer.frame.height)
        alternateContactInfoIcon.frame = CGRect(x: 10.0, y: 488.0, width: alternateContactInfoIcon.frame.width, height: alternateContactInfoIcon.frame.height)
        contactInfoTextField.frame = CGRect(x: 42.0, y: 484.0, width: contactInfoTextField.frame.width, height: contactInfoTextField.frame.height)
    }
    
    func handicapImageIconAdd()
    {
        isDeletingHandicapImage = false
        addHandicapImageLabel.isHidden = false
        
        // show + icon
        addOrRemoveHandicapImageButton.transform = CGAffineTransform.identity
        addOrRemoveHandicapImageButton.tintColor = UIColor.systemGreen
    }
    
    func handicapImageIconRemove()
    {
        isDeletingHandicapImage = true
        addHandicapImageLabel.isHidden = true
        
        //show X icon
        addOrRemoveHandicapImageButton.transform = CGAffineTransform(rotationAngle: CGFloat.pi/4)
        addOrRemoveHandicapImageButton.tintColor = UIColor.systemRed
    }
    
    // KEYBOARD FUNCTIONS
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // Adjust contents in scroll view so keyboard doesn't hide them
    func adjustInsetForKeyboardShow(_ show: Bool, notification: Notification) {
        guard
        let userInfo = notification.userInfo,
        let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey]
          as? NSValue
        else {
          return
      }
        
      let adjustmentHeight = (keyboardFrame.cgRectValue.height + 20) * (show ? 1 : -1)
      scrollView.contentInset.bottom += adjustmentHeight
      scrollView.verticalScrollIndicatorInsets.bottom += adjustmentHeight
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
      adjustInsetForKeyboardShow(true, notification: notification)
    }
    @objc func keyboardWillHide(_ notification: Notification) {
      adjustInsetForKeyboardShow(false, notification: notification)
    }
    
    // PICKER VIEW FUNCTIONS
    
    func createPickerView() {
        let statePickerView = UIPickerView()
        
        statePickerView.delegate = self
        statePickerView.dataSource = self
        statePickerTextField.inputView = statePickerView
    }
       
    func numberOfComponents(in: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ statePicker: UIPickerView, numberOfRowsInComponent: Int) -> Int {
        return STATES.states.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent
        component: Int) -> String? {
        return STATES.states[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedState = String(STATES.states[row].prefix(2))
        statePickerTextField.text = selectedState
        statePickerTextField.textAlignment = .center
    }
    
    // SETTERS
    
    func setContactInfoKeyboard()
    {
        if(selectedContactMethod == PHONE || selectedContactMethod == TEXT)
        {
            contactInfoTextField.keyboardType = UIKeyboardType.numbersAndPunctuation
        }
        else{
            contactInfoTextField.keyboardType = UIKeyboardType.emailAddress
        }
    }
    
    func setPlaceholderText() {
        idTextField.attributedPlaceholder = NSAttributedString(string: "Badge Number*",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        idTextField.textColor = UIColor.black

        licenseTextField.attributedPlaceholder = NSAttributedString(string: "License Plate*",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        licenseTextField.textColor = UIColor.black
        
        contactInfoTextField.attributedPlaceholder = NSAttributedString(string: "Cell number or alternate contact info*",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        contactInfoTextField.textColor = UIColor.black
        
        let style = NSMutableParagraphStyle()
        style.alignment = NSTextAlignment.center
        statePickerTextField.attributedPlaceholder = NSAttributedString(string: "State*",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray,
        .paragraphStyle: style])
        statePickerTextField.textColor = UIColor.black
    }
    
    func setPreferredContactMethod(_ segmentedControl: UISegmentedControl)
    {
        if(segmentedControl.selectedSegmentIndex == 1)
        {
           selectedContactMethod = PHONE
        }
        else if(segmentedControl.selectedSegmentIndex == 2)
        {
            selectedContactMethod = TEXT
        }
        else{
            selectedContactMethod = EMAIL
        }
    }
    
    // CAMERA FUNCTIONS
    
    func selectOrTakePhoto() {
        let imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a source", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                imagePicker.sourceType = .camera;
                self.present(imagePicker, animated: true, completion: nil)
            } else {
                print("Camera not available")
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in imagePicker.sourceType = .photoLibrary; self.present(imagePicker, animated: true, completion: nil)}))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil ))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        if(handicapImage.image == nil){
            handicapImage.image = info[.originalImage] as? UIImage
            handicapImage.isHidden = false
            handicapImageIconRemove()
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func toBase64( image: UIImage ) -> String?{
        let imageData = UIImage.pngData(image)
        return imageData()!.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    // ACTION FUNCTIONS
    
    // makes handicap image cell clickable (only when adding)
    @objc func addHandicapImageLabelClickable(sender:UITapGestureRecognizer)
    {
        addOrRemoveHandicapImage(self)
    }
    
    @IBAction func addOrRemoveHandicapImage(_ sender: Any) {
        if(isDeletingHandicapImage)
        {
            handicapImage.image = nil
            handicapImageIconAdd()
        }
        else {
            selectOrTakePhoto()
        }
    }
    
    @IBAction func contactMethodSelected(_ sender: Any) {
        setPreferredContactMethod(preferredContactMethodSegmentControl)
        setContactInfoKeyboard() //adjust keyboard type depending on contact method
        validContactMethod = true
    }
    
    // Manages selecting and highlighting one color
    @IBAction func selectVehicleColor(_ sender: Any) {
        guard let button = sender as? UIButton else {
            return
        }

        validColor = true
        
        switch button.tag {
        case 1: // black
            clearAndSetColorButtonHighlight(blackButton)
            selectedColor = BLACK
            break
        case 2: // white
            clearAndSetColorButtonHighlight(whiteButton)
            selectedColor = WHITE
            break
        case 3: // gray
            clearAndSetColorButtonHighlight(grayButton)
            selectedColor = GRAY
            break
        case 4: // brown
            clearAndSetColorButtonHighlight(brownButton)
            selectedColor = BROWN
            break
        case 5: // red
            clearAndSetColorButtonHighlight(redButton)
            selectedColor = RED
            break
        case 6: // orange
            clearAndSetColorButtonHighlight(orangeButton)
            selectedColor = ORANGE
            break
        case 7: // yellow
            clearAndSetColorButtonHighlight(yellowButton)
            selectedColor = YELLOW
            break
        case 8: // green
            clearAndSetColorButtonHighlight(greenButton)
            selectedColor = GREEN
            break
        case 9: // blue
            clearAndSetColorButtonHighlight(blueButton)
            selectedColor = BLUE
            break
        case 10: // purple
            clearAndSetColorButtonHighlight(purpleButton)
            selectedColor = PURPLE
            break
        default:
            selectedColor = ""
            validColor = false
        }
    }
    
    @IBAction func takePhoto(_ sender: Any) {
        imagePicker =  UIImagePickerController()
        self.imagePicker.delegate = self
        imagePicker.sourceType = .camera
        present(imagePicker, animated: true, completion: nil)
    }
    
    // Displays or hides add handicap image capability
    @IBAction func toggleHandicapRegistered(_ sender: Any) {
        if(handicapSwitch.isOn)
        {
            showAddHandicapImage()
        }
        else{
            hideAddHandicapImage()
        }
    }
    
    @IBAction func touchBackButton(_ sender: Any) {
        self.performSegue(withIdentifier: "backToHome", sender: nil)
    }
    
    func callVehicleRegistrationAPI() {
        //call vehicle registration API
        //pass in license plate, State and username
        // Call SOAP webservice
        let regNum = self.licenseTextField.text!
        let state = selectedState
        let urlString = "https://www.regcheck.org.uk/api/reg.asmx/CheckUSA?RegistrationNumber=" + regNum + "&State=" + state + "&username=andrea.mathew5555@gmail.com"
        print(urlString)
        var url = NSURL(string: urlString)
        var theRequest = NSMutableURLRequest(url: url! as URL)
        let session = URLSession.shared
        
        theRequest.httpMethod = "GET"
        let task = session.dataTask(with: theRequest as URLRequest, completionHandler: {data, response, error -> Void in
            let strData = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            //print("Body: \(strData)")
            let parser = XMLParser(data: data!)
            parser.delegate = self
            parser.parse()

            if error != nil
            {
                print("Error:")
            }

        })
        task.resume()
        
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        currentParsingElement = elementName
        if elementName == "Vehicle" {
            print("Started parsing...")
        }
    }
      
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        let foundedChar = string.trimmingCharacters(in:NSCharacterSet.whitespacesAndNewlines)

        if (!foundedChar.isEmpty) {
            if currentParsingElement == "RegistrationYear" {
                self.year = foundedChar
                print("From parser")
                print(self.year)
            }
            if currentParsingElement == "Description" {
                self.make = foundedChar
                print("From parser")
                print(self.year)
            }
        }
          
    }
      
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "Vehicle" {
            print("Ended parsing...")
              
        }
    }
      
    func parserDidEndDocument(_ parser: XMLParser) {
        print("End of doc")
    }
      
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        print("parseErrorOccurred: \(parseError)")
    }
    
    @IBAction func submit(_ sender: Any) {
        if(validBadge && validLicense && validState && validColor && validContactMethod && validContactInfo)
        {
            submitTicket()
        }
        else
        {
            self.performSegue(withIdentifier: "formRequestFailed", sender: nil)
        }
    }
    
    // POST API REQUEST
    
    func submitTicket() {
        self.callVehicleRegistrationAPI()
        sleep(3)
        print("YEAR AND MAKE:")
        print(self.year)
        print(self.make)
        
        let getTicketEndpoint: String = "https://modular-ground-255216.appspot.com/create_parking_pass"

        guard let url = URL(string: getTicketEndpoint) else {
            print("Error: cannot create URL")
            return
            }

        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"

        let hImage : String = ""
        if(handicapImage.image != nil){
            let imageCompress = handicapImage.image!.jpegData(compressionQuality: 0.50)
            let hImage: String? = (imageCompress?.base64EncodedString())!
        }
        
        // TODO edit hardcoded fields
        let json = [
            "user_id": "asm375",
            "handicap_image": handicapSwitch.isOn ? hImage : "",
            "is_ev": electricVehicleSwitch.isOn,
            "badge_no": idTextField.text!,
            "first_name": "Andrea",
            "last_name": "Mathew",
            "email": "email@email.com",
            "multiple": multipleVehicleSwitch.isOn,
            "pref_contact": selectedContactMethod,
            "alt_contact": contactInfoTextField.text!,
            "is_contractor": false,
            "color": selectedColor,
            "license_plate": licenseTextField.text!,
            "state": selectedState,
            "year": Int(self.year),
            "make": self.make,
            "model": "model",
            "is_handicap": handicapSwitch.isOn
        ] as [String: Any]

        let jsonData = try? JSONSerialization.data(withJSONObject: json)

        urlRequest.httpBody = jsonData

        urlRequest.setValue("\(jsonData?.count)", forHTTPHeaderField: "Content-Length")
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")

        let session = URLSession.shared
        self.dispatchGroup.enter()
        self.submitButton.isEnabled = false;
        
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            guard error == nil else {

                print("error calling GET")
                print(error!)
                return
            }
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            do {
                let response = try JSONSerialization.jsonObject(with: responseData, options:[])
                self.dispatchGroup.leave()
                print(response)
            } catch  {
                print("error trying to convert data to JSON")
                self.submitButton.isEnabled = false;
                return
            }
        }
        task.resume()
        
        self.dispatchGroup.notify(queue: .main) {
            self.performSegue(withIdentifier: "thankYou", sender: nil)
        }
    }
}

