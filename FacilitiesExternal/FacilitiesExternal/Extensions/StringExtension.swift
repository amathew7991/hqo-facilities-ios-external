//
//  StringExtension.swift
//  FacilitiesExternal
//
//  Created by Justin Perez on 2/21/20.
//  Copyright © 2020 Andrea Mathew. All rights reserved.
//

extension String {
    var isInt: Bool {
        return Int(self) != nil
    }
    
    func matches(_ regex: String) -> Bool {
        return self.range(of: regex, options: .regularExpression, range: nil, locale: nil) != nil
    }
}
